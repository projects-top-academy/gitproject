#include <iostream>

double func(double a, int b){
    return a + b;
}

int main() {
    double a = 12.2;
    int b = 10;
    std::cout << func(a, b);
    return 0;
}
